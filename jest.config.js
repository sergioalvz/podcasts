module.exports = {
  moduleNameMapper: {
    "\\.css$": "<rootDir>/test/mocks/styleMock.js",
    "\\.svg$": "<rootDir>/test/mocks/svgMock.js",
  },
  setupFiles: ["<rootDir>/jest.setup.js"],
  transform: {
    "^.+\\.jsx?$": "babel-jest",
  },
};
