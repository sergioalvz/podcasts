module.exports = {
  arrowParens: "always",
  printWidth: 120,
  trailingComma: "all",
  overrides: [
    {
      files: "*.md",
      options: {
        tabWidth: 4,
      },
    },
  ],
};
