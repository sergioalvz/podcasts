# podcasts

Single-page application built with [React](https://reactjs.org/) to display a collection of podcasts.

---

-   [How-to](#how-to)
    -   [How to install dependencies](#how-to-install-dependencies)
    -   [How to run the application in _dev_ mode](#how-to-run-the-application-in-_dev_-mode)
    -   [How to run the application in _production_ mode](#how-to-run-the-application-in-_production_-mode)
    -   [How to run the test suite](#how-to-run-the-test-suite)
    -   [How to lint the project](#how-to-lint-the-project)
-   [Reasoning](#reasoning)
    -   [State management](#state-management)
    -   [localStorage](#localstorage)
    -   [Smoke testing](#smoke-testing)
-   [Possible improvements / alternate solutions](#possible-improvements--alternate-solutions)
    -   [Service workers](#service-workers)
    -   [Cache @ memory-level](#cache--memory-level)
-   [Errors](#errors)
    -   [RSS Parsing](#rss-parsing)

---

## How-to

### How to install dependencies

This project makes use of `npm` as a package manager. Typing `npm install` will install all the required dependencies defined at `package.json`.

### How to run the application in _dev_ mode

There is a `npm script` for that. Just type the following command on your terminal of choice:

```sh
npm run dev
```

This will spin up a web server at <http://localhost:1234> to browse the application.

### How to run the application in _production_ mode

It is possible to build the application for production by typing: `npm run build`. This will place the resulted files at `./dist` (minified, concatenated and so on).

To browse the production-like site there are a couple of alternatives:

-   Make use of [http-server](https://www.npmjs.com/package/http-server) (or similar tools) to run a local web server on the `./dist` folder: `npx http-server dist`
-   Go to <https://podcaster.netlify.com/>. [Netlify](https://www.netlify.com/) serves as a CDN so the assets are also _gzipped_ for maximum performance. This site is automatically deployed on every _push_ into `master`.

### How to run the test suite

This project uses [jest](https://jestjs.io/) as testing framework and test runner. Typing the following command will run the whole test suite.

```sh
npm run test
```

It is possible as well to execute independent suites by using the following commands:

```sh
npm run test:acceptance # runs the acceptance tests only
npm run test:smoke # runs the smoke tests only
npm run test:unit # runs the unitary tests only
```

### How to lint the project

Type `npm run lint` to get a report of possible violations of linting rules. This project makes use of [ESLint](https://eslint.org/) and some other plug-ins to analyze JavaScript and JSX code.

## Reasoning

### State management

Although [Redux](https://redux.js.org/) is a _de facto_ library to manage state in React I found the proposed approach on this solution to be quite simpler and easier to understand. The _actions_ and _reducers_ could be mapped here on `PodcastRepository` which is passed down the tree of components by using `props` instead of the regular high-order component used by [`react-redux`](https://github.com/reduxjs/react-redux).

The repository's interface offers three methods to let components to access the _store_ without needing to know its internal state or implementation. This façade could be easily integrated with Redux actions and reducers if it gets added afterwards.

This overall strategy led to an easy-to-test design which is fully covered by unitary tests.

### localStorage

`localStorage` is the storage service used to save cached responses from the service. It provides a simple API which is easy to use, mock and test. Since there were not any security-related requirements, this approach provide a store with [dynamic storage limits](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API/Browser_storage_limits_and_eviction_criteria#Storage_limits).

### Smoke testing

All major components have a simple smoke test that ensure that it is not breaking on render-time. This approach should give quick feedback about big failures that may lead to the application to be broken.

## Possible improvements / alternate solutions

### Service workers

Dependending on how critical the level of cache is to avoid multiple HTTP requests to the same resource within the same day, using service workers might be a handy solution to avoid mixing that cache-related logic with business logic on the same codebase.

Since no critical code can be placed into service workers (because they might not be available in all client devices) these are changes that could be applied later like some how of progressive enhancement.

### Cache @ memory-level

The `PodcastRepository` abstraction opens a door to include a new layer of cache at the memory-level by memoizing the result of each `all` and `findById` operation.

## Errors

### RSS Parsing

Some podcasts return an invalid RSS document which cannot be parsed by [`rss-parser`](https://www.npmjs.com/package/rss-parser). In those cases, a stack trace is printed on the Console and the _Loading_ screen does not disappear.

---

Author [Sergio Álvarez](https://twitter.com/codecoolture). Licensed under Apache-2.0.
