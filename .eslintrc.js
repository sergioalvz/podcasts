module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
  },
  extends: ["eslint:recommended", "plugin:react/recommended", "plugin:prettier/recommended", "prettier/react"],
  parser: "babel-eslint",
  parserOptions: {
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ["prettier", "react"],
  rules: {
    "no-console": "off",
    "prettier/prettier": "error",
  },
  settings: {
    react: {
      version: "16.6.0", // React version
    },
  },
};
