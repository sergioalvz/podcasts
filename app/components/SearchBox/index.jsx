import React from "react";
import { func, number, string } from "prop-types";

import "./style.css";

export function SearchBox({ className, numberOfResults, onChange, placeholder }) {
  const classes = ["SearchBox", className].filter(Boolean).join(" ");

  return (
    <label className={classes}>
      <span className="SearchBox__Result" data-test-id="results">
        {numberOfResults}
      </span>

      <input
        className="SearchBox__Input"
        data-test-id="input"
        onChange={onChange}
        placeholder={placeholder}
        type="text"
      />
    </label>
  );
}

SearchBox.propTypes = {
  className: string,
  numberOfResults: number.isRequired,
  onChange: func.isRequired,
  placeholder: string.isRequired,
};
