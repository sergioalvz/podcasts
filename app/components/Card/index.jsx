import React from "react";
import { node, string } from "prop-types";

import "./style.css";

export function Card({ children, className }) {
  const classes = ["Card", className].filter(Boolean).join(" ");

  return <div className={classes}>{children}</div>;
}

Card.propTypes = {
  children: node.isRequired,
  className: string,
};
