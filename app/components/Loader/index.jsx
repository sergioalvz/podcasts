import React from "react";
import { string } from "prop-types";

import spinner from "./img/spinner.svg";

import "./style.css";

export function Loader({ label }) {
  return (
    <div className="Loader">
      <div dangerouslySetInnerHTML={{ __html: spinner }} />
      <p className="Loader__Text">{label}</p>
    </div>
  );
}

Loader.propTypes = {
  label: string.isRequired,
};
