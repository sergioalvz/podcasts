export default {
  common: {
    loading: "Loading...",
  },
  layouts: {
    podcast: {
      information: {
        artist: "by {{artist}}",
        description: "Description",
      },
    },
  },
  screens: {
    podcast: {
      heading: "Episodes: {{numberOfEpisodes}}",
      table: {
        date: "Date",
        duration: "Duration",
        title: "Title",
      },
    },
    podcasts: {
      author: "Author: {{author}}",
      search: {
        placeholder: "Filter podcasts...",
      },
    },
  },
};
