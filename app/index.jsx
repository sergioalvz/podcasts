import React from "react";
import { render } from "react-dom";

import { App } from "./App";
import { podcastRepository } from "./entities/repositories";

render(<App podcastRepository={podcastRepository} />, document.querySelector(".js-app"));
