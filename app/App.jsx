import { instanceOf } from "prop-types";
import React from "react";
import { I18nextProvider } from "react-i18next";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import { i18n } from "./config/i18n";
import { PodcastRepository } from "./entities/repositories/PodcastRepository";
import { Layout } from "./layouts/Layout";
import { Episode } from "./screens/Episode";
import { Podcast } from "./screens/Podcast";
import { Podcasts } from "./screens/Podcasts";

import "./style.css";

export function App({ podcastRepository }) {
  const render = (Screen) => {
    return function Page(props) {
      return <Screen {...props} podcastRepository={podcastRepository} />;
    };
  };

  return (
    <I18nextProvider i18n={i18n}>
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route path="/" exact render={render(Podcasts)} />
            <Route path="/podcast/:podcastId" exact render={render(Podcast)} />
            <Route path="/podcast/:podcastId/episode/:episodeId" exact render={render(Episode)} />
          </Switch>
        </Layout>
      </BrowserRouter>
    </I18nextProvider>
  );
}

App.propTypes = {
  podcastRepository: instanceOf(PodcastRepository).isRequired,
};
