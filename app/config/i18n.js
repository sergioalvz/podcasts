import i18n from "i18next";

import en from "../locales/en";

i18n.init({
  fallbackLng: "en",
  debug: false,
  interpolation: { escapeValue: false },
  resources: { en },
});

export { i18n };
