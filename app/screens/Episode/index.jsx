import { func, instanceOf, shape, string } from "prop-types";
import React, { Component } from "react";
import { withNamespaces } from "react-i18next";

import { Podcast as PodcastLayout } from "../../layouts/Podcast";
import { Loader } from "../../components/Loader";
import { PodcastRepository } from "../../entities/repositories/PodcastRepository";

import "./style.css";

class EpisodeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = { episode: null, podcast: null };
  }

  async componentDidMount() {
    const { episodeId, podcastId } = this.props.match.params;

    const podcast = await this.props.podcastRepository.findById(podcastId);
    const episode = podcast.episodes.find((episodeOfPodcast) => episodeOfPodcast.id === episodeId);

    this.setState({ episode, podcast });
  }

  render() {
    const { episode, podcast } = this.state;

    if (!episode) {
      return <Loader label={this.props.t("common:loading")} />;
    }

    return (
      <PodcastLayout
        podcast={podcast}
        top={
          <PodcastLayout.Card>
            <h1 className="Episode__Heading">{episode.title}</h1>
            <div dangerouslySetInnerHTML={{ __html: episode.description }} />

            <audio controls src={episode.url} className="Episode__Player" />
          </PodcastLayout.Card>
        }
      />
    );
  }
}

EpisodeScreen.propTypes = {
  match: shape({
    params: shape({
      podcastId: string.isRequired,
      episodeId: string.isRequired,
    }).isRequired,
  }).isRequired,
  podcastRepository: instanceOf(PodcastRepository).isRequired,
  t: func.isRequired,
};

export const Episode = withNamespaces()(EpisodeScreen);
