import React from "react";
import { func, instanceOf } from "prop-types";
import { Link } from "react-router-dom";

import { Podcast } from "../../../../entities/Podcast";
import { Card } from "../../../../components/Card";

import "./style.css";

export function PodcastCover({ podcast, t }) {
  return (
    <Link className="PodcastCover" to={`/podcast/${podcast.id}`}>
      <Card className="PodcastCover__Card">
        <img className="PodcastCover__Image" src={podcast.image} alt={`Image for ${podcast.name}`} />
        <h1 className="PodcastCover__Title">{podcast.name}</h1>
        <h2 className="PodcastCover__Subtitle">{t("podcasts.author", { author: podcast.artist })}</h2>
      </Card>
    </Link>
  );
}

PodcastCover.propTypes = {
  podcast: instanceOf(Podcast),
  t: func.isRequired,
};
