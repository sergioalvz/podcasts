import React from "react";
import { arrayOf, func, instanceOf } from "prop-types";

import { Podcast } from "../../../../entities/Podcast";

import "./style.css";
import { PodcastCover } from "../PodcastCover";

export function PodcastGrid({ podcasts, t }) {
  return (
    <section className="PodcastGrid">
      {podcasts.map((podcast, idx) => (
        <PodcastCover key={idx} podcast={podcast} t={t} />
      ))}
    </section>
  );
}

PodcastGrid.propTypes = {
  podcasts: arrayOf(instanceOf(Podcast)).isRequired,
  t: func.isRequired,
};
