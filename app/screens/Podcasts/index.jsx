import debounce from "lodash/debounce";
import { func, instanceOf } from "prop-types";
import React, { Component } from "react";
import { withNamespaces } from "react-i18next";

import { SearchBox } from "../../components/SearchBox";
import { PodcastGrid } from "./components/PodcastGrid";
import { Loader } from "../../components/Loader";
import { PodcastRepository } from "../../entities/repositories/PodcastRepository";

import "./style.css";

class PodcastsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = { podcasts: null };

    this.onSearch = debounce(this.onSearch.bind(this));
  }

  async componentDidMount() {
    const podcasts = await this.props.podcastRepository.all();

    this.setState({ podcasts });
  }

  async onSearch(value) {
    const podcasts = await this.props.podcastRepository.search(value);

    this.setState({ podcasts });
  }

  render() {
    const { podcasts } = this.state;

    if (!podcasts) {
      return <Loader label={this.props.t("common:loading")} />;
    }

    return (
      <section className="Podcasts">
        <SearchBox
          className="Podcasts__SearchBox"
          numberOfResults={podcasts.length}
          placeholder={this.props.t("podcasts.search.placeholder")}
          onChange={(event) => this.onSearch(event.currentTarget.value)}
        />

        <PodcastGrid podcasts={podcasts} t={this.props.t} />
      </section>
    );
  }
}

PodcastsScreen.propTypes = {
  podcastRepository: instanceOf(PodcastRepository).isRequired,
  t: func.isRequired,
};

export const Podcasts = withNamespaces("screens")(PodcastsScreen);
