import { arrayOf, instanceOf, func } from "prop-types";
import format from "date-fns/format";
import React from "react";
import { Link } from "react-router-dom";

import { Episode } from "../../../../entities/Episode";

import "./style.css";

export function Episodes({ episodes, t }) {
  return (
    <table className="Episodes">
      <thead>
        <tr className="Episodes__Header">
          <th className="Episodes__Title Episodes__Cell">{t("podcast.table.title")}</th>
          <th className="Episodes__Date Episodes__Cell">{t("podcast.table.date")}</th>
          <th className="Episodes__Duration Episodes__Cell">{t("podcast.table.duration")}</th>
        </tr>
      </thead>
      <tbody>
        {episodes.map((episode, idx) => {
          return (
            <tr key={idx} className="Episodes__Item">
              <td className="Episodes__Cell">
                <Link className="Episodes__Link" to={`/podcast/${episode.podcastId}/episode/${episode.id}`}>
                  {episode.title}
                </Link>
              </td>

              <td className="Episodes__Cell">{format(episode.releaseDate, "D/M/YYYY")}</td>

              <td className="Episodes__Duration Episodes__Cell">{episode.duration}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

Episodes.propTypes = {
  episodes: arrayOf(instanceOf(Episode)),
  t: func.isRequired,
};
