import { func, instanceOf, shape, string } from "prop-types";
import React, { Component } from "react";
import { withNamespaces } from "react-i18next";

import { Episodes } from "./components/Episodes";
import { Podcast as PodcastLayout } from "../../layouts/Podcast";
import { Loader } from "../../components/Loader";
import { PodcastRepository } from "../../entities/repositories/PodcastRepository";

import "./style.css";

class PodcastScreen extends Component {
  constructor(props) {
    super(props);

    this.state = { podcast: null };
  }

  async componentDidMount() {
    const { podcastId } = this.props.match.params;

    const podcast = await this.props.podcastRepository.findById(podcastId);

    this.setState({ podcast });
  }

  render() {
    const { podcast } = this.state;

    if (!podcast) {
      return <Loader label={this.props.t("common:loading")} />;
    }

    return (
      <PodcastLayout
        podcast={podcast}
        top={
          <PodcastLayout.Card>
            <p className="Podcast__Display">
              {this.props.t("podcast.heading", { numberOfEpisodes: podcast.episodes.length })}
            </p>
          </PodcastLayout.Card>
        }
        bottom={
          <PodcastLayout.Card>
            <Episodes episodes={podcast.episodes} t={this.props.t} />
          </PodcastLayout.Card>
        }
      />
    );
  }
}

PodcastScreen.propTypes = {
  match: shape({
    params: shape({
      podcastId: string.isRequired,
    }).isRequired,
  }).isRequired,
  podcastRepository: instanceOf(PodcastRepository).isRequired,
  t: func.isRequired,
};

export const Podcast = withNamespaces("screens")(PodcastScreen);
