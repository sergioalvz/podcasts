import { Episode } from "./Episode";

export class Podcast {
  constructor({ name, artist, image, id, description, episodes = [] }) {
    this.name = name;
    this.artist = artist;
    this.image = image;
    this.id = id;
    this.description = description;
    this.episodes = episodes;
  }
}

Podcast.fromItunesJson = (json) => {
  return new Podcast({
    name: json["im:name"]["label"],
    artist: json["im:artist"]["label"],
    image: json["im:image"][2]["label"],
    id: json["id"]["attributes"]["im:id"],
  });
};

Podcast.fromFeedJson = (id, { description, title, itunes: { author, image }, items }) => {
  return new Podcast({
    name: title,
    artist: author,
    image,
    id,
    description,
    episodes: items.map((episodeJson) => Episode.fromFeedJson(id, episodeJson)),
  });
};
