import slugify from "slugify";

export class Episode {
  constructor({ description, duration, id, podcastId, releaseDate, title, url }) {
    this.description = description;
    this.duration = duration;
    this.id = id;
    this.podcastId = podcastId;
    this.releaseDate = releaseDate;
    this.title = title;
    this.url = url;
  }
}

Episode.fromFeedJson = (podcastId, { title, isoDate, content, enclosure: { url } = {}, itunes: { duration } = {} }) => {
  return new Episode({
    description: content,
    duration,
    id: slugify(title, { lower: true }),
    podcastId,
    releaseDate: new Date(isoDate),
    title,
    url,
  });
};
