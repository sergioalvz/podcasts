import { PodcastRepository } from "./PodcastRepository";
import { podcasts } from "./getters";
import { podcast } from "./getters";

const podcastRepository = PodcastRepository.build({ collectionGetter: podcasts, itemGetter: podcast });

export { podcastRepository };
