import RssParser from "rss-parser";

import { Podcast } from "../../Podcast";

export function podcastGetterFactory({ cache }) {
  return async ({ id }) => {
    let feed;

    if (cache.hasValidCachedData({ id })) {
      feed = cache.getCachedData({ id });
    } else {
      const baseUrl = "https://cors-anywhere.herokuapp.com";

      const lookupResponse = await fetch(`${baseUrl}/https://itunes.apple.com/lookup?id=${id}`);
      const detailedPodcastJson = await lookupResponse.json();
      const { feedUrl } = detailedPodcastJson["results"][0];

      const feedResponse = await fetch(`${baseUrl}/${feedUrl}`);
      const rss = await feedResponse.text();

      feed = await new RssParser().parseString(rss);

      cache.persist({ id, data: feed });
    }

    return Podcast.fromFeedJson(id, feed);
  };
}
