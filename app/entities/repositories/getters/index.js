import { podcastsGetterFactory } from "./podcasts";
import { podcastGetterFactory } from "./podcast";
import { podcasts as podcastsCache } from "./caches/podcasts";
import { podcast as podcastCache } from "./caches/podcast";

const podcasts = podcastsGetterFactory({ cache: podcastsCache({ storage: localStorage, today: new Date() }) });
const podcast = podcastGetterFactory({ cache: podcastCache({ storage: localStorage, today: new Date() }) });

export { podcasts, podcast };
