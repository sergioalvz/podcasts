import differenceInDays from "date-fns/difference_in_days";

export function podcast({ storage = localStorage, today = new Date() }) {
  return {
    hasValidCachedData({ id }) {
      const json = storage.getItem(`podcaster.podcast.${id}`);
      const { lastUpdateAt } = JSON.parse(json || "{}");

      if (!lastUpdateAt) {
        return false;
      }

      return differenceInDays(today, new Date(lastUpdateAt)) < 1;
    },

    getCachedData({ id }) {
      const json = storage.getItem(`podcaster.podcast.${id}`);

      if (!json) {
        return undefined;
      }

      const { data } = JSON.parse(json);

      return data;
    },

    persist({ id, data }) {
      if (!id) {
        throw new Error("An id property was expected but it is undefined");
      }

      if (!data || typeof data !== "object") {
        throw new Error("Invalid data to persist");
      }

      try {
        storage.setItem(`podcaster.podcast.${id}`, JSON.stringify({ lastUpdateAt: today, data }));
      } catch (error) {
        // storage#setItem could potentially throw a DOMException if application
        // exceeds available storage quota.
        console.error("There was an error when trying to persist some cached data", error);
      }
    },
  };
}
