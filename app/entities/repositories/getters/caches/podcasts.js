import differenceInDays from "date-fns/difference_in_days";

export function podcasts({ storage = localStorage, today = new Date() }) {
  return {
    hasValidCachedData() {
      const json = storage.getItem("podcaster.podcasts");
      const { lastUpdateAt } = JSON.parse(json || "{}");

      if (!lastUpdateAt) {
        return false;
      }

      return differenceInDays(today, new Date(lastUpdateAt)) < 1;
    },

    getCachedData() {
      const json = storage.getItem("podcaster.podcasts");

      if (!json) {
        return undefined;
      }

      const { data } = JSON.parse(json);

      return data;
    },

    persist({ data }) {
      if (!data || typeof data !== "object") {
        throw new Error("Invalid data to persist");
      }

      try {
        storage.setItem("podcaster.podcasts", JSON.stringify({ lastUpdateAt: today, data }));
      } catch (error) {
        // storage#setItem could potentially throw a DOMException if application
        // exceeds available storage quota.
        console.error("There was an error when trying to persist some cached data", error);
      }
    },
  };
}
