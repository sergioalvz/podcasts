import { Podcast } from "../../Podcast";

export function podcastsGetterFactory({ cache }) {
  return async () => {
    let json;

    if (cache.hasValidCachedData()) {
      json = cache.getCachedData();
    } else {
      const response = await fetch("https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json");
      json = await response.json();
      cache.persist({ data: json });
    }

    return json.feed.entry.map((podcast) => Podcast.fromItunesJson(podcast));
  };
}
