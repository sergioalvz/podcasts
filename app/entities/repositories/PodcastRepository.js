export class PodcastRepository {
  constructor({ collectionGetter, itemGetter }) {
    this._collectionGetter = collectionGetter;
    this._itemGetter = itemGetter;
  }

  all() {
    return this._collectionGetter();
  }

  findById(id) {
    return this._itemGetter({ id });
  }

  async search(text) {
    const podcasts = await this.all();

    return podcasts.filter((podcast) => {
      return ["name", "artist"].some((key) => podcast[key].toLowerCase().indexOf(text.toLowerCase()) !== -1);
    });
  }
}

PodcastRepository.build = ({ collectionGetter, itemGetter }) => new PodcastRepository({ collectionGetter, itemGetter });
