import { func, instanceOf, node, string } from "prop-types";
import React from "react";
import { withNamespaces } from "react-i18next";
import { Link } from "react-router-dom";

import { Podcast as PodcastEntity } from "../../entities/Podcast";
import { Card } from "../../components/Card";

import "./style.css";

function PodcastCard({ children, className }) {
  const classes = ["PodcastLayout__Card", className].filter(Boolean).join(" ");

  return <Card className={classes}>{children}</Card>;
}

PodcastCard.propTypes = {
  children: node.isRequired,
  className: string,
};

function PodcastLayout({ bottom, podcast, t, top }) {
  return (
    <section className="PodcastLayout">
      <Podcast.Card className="PodcastLayout__Left">
        <Link className="PodcastLayout__Cover" to={`/podcast/${podcast.id}`}>
          <img className="PodcastLayout__Img" src={podcast.image} alt={`Cover image for ${podcast.name}`} />
        </Link>

        <hr className="PodcastLayout__Separator" />

        <h1 className="PodcastLayout__Title">
          <Link className="PodcastLayout__Link" to={`/podcast/${podcast.id}`}>
            {podcast.name}
          </Link>
        </h1>

        <h2 className="PodcastLayout__Subtitle">
          <Link className="PodcastLayout__Link" to={`/podcast/${podcast.id}`}>
            {t("podcast.information.artist", { artist: podcast.artist })}
          </Link>
        </h2>

        <hr className="PodcastLayout__Separator" />

        <h3 className="PodcastLayout__Heading">{t("podcast.information.description")}</h3>

        <div className="PodcastLayout__Description" dangerouslySetInnerHTML={{ __html: podcast.description }} />
      </Podcast.Card>

      <section className="PodcastLayout__Top">{top}</section>

      <section className="PodcastLayout__Bottom">{bottom}</section>
    </section>
  );
}

PodcastLayout.Card = PodcastCard;

PodcastLayout.propTypes = {
  bottom: node,
  children: node.isRequired,
  podcast: instanceOf(PodcastEntity).isRequired,
  t: func.isRequired,
  top: node,
};

export const Podcast = withNamespaces("layouts")(PodcastLayout);
