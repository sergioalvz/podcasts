import debounce from "lodash/debounce";
import React, { Component, Fragment } from "react";
import { withRouter, Link } from "react-router-dom";
import { node, object } from "prop-types";

import { Spinner } from "./components/Spinner";

import "./style.css";

const ESTIMATED_TRANSITION_OFFSET_TIME = 500;

class LayoutComponent extends Component {
  constructor(props) {
    super(props);

    this.stopTransitionSpinner = debounce(
      () => this.setState({ transitioning: false }),
      ESTIMATED_TRANSITION_OFFSET_TIME,
    );

    this.state = { transitioning: false };

    this.onHistoryChange = this.onHistoryChange.bind(this);
  }

  componentDidMount() {
    this.props.history.listen(this.onHistoryChange);
  }

  onHistoryChange() {
    this.stopTransitionSpinner.cancel();

    this.setState({ transitioning: true });

    this.stopTransitionSpinner();
  }

  render() {
    return (
      <Fragment>
        <div className="Layout">
          <header className="Layout__Header LayoutHeader">
            <h1 className="LayoutHeader__Title">
              <Link className="LayoutHeader__Link" to="/">
                Podcaster
              </Link>
            </h1>

            {this.state.transitioning ? <Spinner /> : null}
          </header>

          <main className="Layout__Main">{this.props.children}</main>
        </div>
      </Fragment>
    );
  }
}

LayoutComponent.propTypes = {
  children: node.isRequired,
  history: object,
};

export const Layout = withRouter(LayoutComponent);
