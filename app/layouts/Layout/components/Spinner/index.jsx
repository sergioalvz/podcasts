import React from "react";

import "./style.css";

export function Spinner() {
  return (
    <div className="Spinner ball-scale">
      <div />
    </div>
  );
}
