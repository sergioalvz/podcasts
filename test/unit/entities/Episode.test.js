import { readFileSync } from "fs";
import { join } from "path";
import RssParser from "rss-parser";

import { Episode } from "../../../app/entities/Episode";

describe("Podcast", () => {
  describe(".fromFeedJson", async () => {
    it("builds an Episode object from a JSON feed representation", async () => {
      const rss = readFileSync(join(__dirname, "../../fixtures/api/rss.xml"), "utf-8");
      const feedJson = await new RssParser().parseString(rss);
      const episodeJson = feedJson.items.shift();
      const podcastId = "123456789";

      const episode = Episode.fromFeedJson(podcastId, episodeJson);
      expect(episode.id).toEqual("bollywood-chronicles-e9-before-sunset-or-bollywood-summer-mix-2018");
      expect(episode.title).toEqual("Bollywood Chronicles E9 - Before Sunset | Bollywood Summer Mix 2018");
      expect(episode.duration).toEqual("00:56:59");
      expect(episode.description).toEqual(expect.any(String));
      expect(episode.url).toEqual(
        "http://feeds.soundcloud.com/stream/516474372-bollywoodchronicles-bollywood-chronicles-e9-before-sunset-bollywood-summer-mix-2018.mp3",
      );
      expect(episode.releaseDate).toEqual(new Date("Fri, 19 Oct 2018 00:57:07 +0000"));
      expect(episode.podcastId).toEqual("123456789");
    });
  });
});
