import { readFileSync } from "fs";
import { join } from "path";
import RssParser from "rss-parser";

import { Podcast } from "../../../app/entities/Podcast";
import { Episode } from "../../../app/entities/Episode";

describe("Podcast", () => {
  describe(".fromItunesJson", () => {
    it("builds a Podcast object from a JSON representation", () => {
      const json = JSON.parse(readFileSync(join(__dirname, "../../fixtures/api/podcast.json"), "utf-8"));

      const podcast = Podcast.fromItunesJson(json);
      expect(podcast.name).toEqual("Bollywood Chronicles");
      expect(podcast.artist).toEqual("DJ MRA");
      expect(podcast.image).toEqual(
        "https://is3-ssl.mzstatic.com/image/thumb/Podcasts118/v4/6d/37/7a/6d377a32-d75c-cbdd-8a6c-f26a37458f5c/mza_4217871426333035028.jpg/170x170bb-85.png",
      );
      expect(podcast.id).toEqual("1083608200");
    });
  });

  describe(".fromFeedJson", async () => {
    it("builds a Podcast object from a JSON feed representation", async () => {
      const rss = readFileSync(join(__dirname, "../../fixtures/api/rss.xml"), "utf-8");
      const feedJson = await new RssParser().parseString(rss);

      const podcast = Podcast.fromFeedJson("123456789", feedJson);
      expect(podcast.name).toEqual("Bollywood Chronicles");
      expect(podcast.artist).toEqual("DJ MRA");
      expect(podcast.image).toEqual("http://i1.sndcdn.com/avatars-000528411912-dqg01k-original.jpg");
      expect(podcast.id).toEqual("123456789");
      expect(podcast.description).toEqual(expect.any(String));

      expect(podcast.episodes).toHaveLength(14);
      expect(podcast.episodes).toEqual(expect.arrayContaining([expect.any(Episode)]));
    });
  });
});
