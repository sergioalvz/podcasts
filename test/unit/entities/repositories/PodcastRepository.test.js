import { PodcastRepository } from "../../../../app/entities/repositories/PodcastRepository";
import { Podcast } from "../../../../app/entities/Podcast";
import { PodcastFactory } from "../../../factories/Podcast";

describe("PodcastRepository", () => {
  describe("#all", () => {
    it("uses the collection getter to retrieve the items", async () => {
      const collectionGetter = jest.fn(() => Promise.resolve([PodcastFactory()]));
      const subject = new PodcastRepository({ collectionGetter });

      await subject.all();

      expect(collectionGetter).toHaveBeenCalled();
    });

    it("returns a collection of Podcast objects", async () => {
      const collectionGetter = jest.fn(() => Promise.resolve([PodcastFactory()]));
      const subject = new PodcastRepository({ collectionGetter });

      const result = await subject.all();

      expect(result).toEqual(expect.arrayContaining([expect.any(Podcast)]));
    });
  });

  describe("#search", () => {
    it("returns a collection of objects that match the query", async () => {
      const collectionGetter = jest.fn(() => Promise.resolve([PodcastFactory({ name: "My awesome Podcast" })]));
      const subject = new PodcastRepository({ collectionGetter });

      const result = await subject.search("My");

      expect(result).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            name: expect.stringContaining("My"),
          }),
        ]),
      );
    });

    it("works independently from query casing", async () => {
      const collectionGetter = jest.fn(() => Promise.resolve([PodcastFactory({ name: "My awesome Podcast" })]));
      const subject = new PodcastRepository({ collectionGetter });

      const result = await subject.search("mY");

      expect(result).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            name: expect.stringContaining("My"),
          }),
        ]),
      );
    });

    it("does not return anything if the query matches no podcast", async () => {
      const collectionGetter = jest.fn(() => Promise.resolve([PodcastFactory({ name: "My awesome Podcast" })]));
      const subject = new PodcastRepository({ collectionGetter });

      const result = await subject.search("FOO");

      expect(result).toEqual([]);
    });

    it("does search by podcast name as well as podcast artist", async () => {
      const collectionGetter = jest.fn(() => Promise.resolve([PodcastFactory({ artist: "Awesome artist" })]));
      const subject = new PodcastRepository({ collectionGetter });

      const result = await subject.search("art");

      expect(result).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            artist: expect.stringContaining("art"),
          }),
        ]),
      );
    });

    it("works with everything playing together", async () => {
      const collectionGetter = jest.fn(() =>
        Promise.resolve([
          PodcastFactory({ artist: "Incredible artist" }),
          PodcastFactory({ name: "Another incredible podcast" }),
          PodcastFactory({ name: "Whatever" }),
        ]),
      );

      const subject = new PodcastRepository({ collectionGetter });

      const result = await subject.search("incredible");

      expect(result).toHaveLength(2);

      expect(result).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            artist: expect.stringContaining("Incredible"),
          }),
          expect.objectContaining({
            name: expect.stringContaining("incredible"),
          }),
        ]),
      );
    });
  });

  describe("#findById", () => {
    it("uses the item getter to retrieve the collection", async () => {
      const itemGetter = jest.fn(() => Promise.resolve(PodcastFactory()));
      const subject = new PodcastRepository({ itemGetter });

      await subject.findById("123456789");

      expect(itemGetter).toHaveBeenCalledWith({ id: "123456789" });
    });

    it("returns a Podcast object", async () => {
      const itemGetter = jest.fn(() => Promise.resolve(PodcastFactory()));
      const subject = new PodcastRepository({ itemGetter });

      const result = await subject.findById("123456789");

      expect(result).toBeInstanceOf(Podcast);
    });

    it("throws an exception if the podcast is not found", async () => {
      expect.assertions(1);

      const itemGetter = jest.fn(Promise.reject);
      const subject = new PodcastRepository({ itemGetter });

      try {
        await subject.findById("123456789");
      } catch (error) {
        expect(error).toBeDefined();
      }
    });
  });
});
