import { readFileSync } from "fs";
import { join } from "path";
import RssParser from "rss-parser";
import noop from "lodash/noop";

import { podcastGetterFactory } from "../../../../../app/entities/repositories/getters/podcast";
import { Podcast } from "../../../../../app/entities/Podcast";

describe("getter", () => {
  describe("podcast", () => {
    const json = JSON.parse(readFileSync(join(__dirname, "../../../../fixtures/api/lookup.json"), "utf-8"));
    const rss = readFileSync(join(__dirname, "../../../../fixtures/api/rss.xml"), "utf-8");

    let cache;

    beforeEach(() => {
      cache = { hasValidCachedData: () => false, getCachedData: () => noop, persist: jest.fn() };

      global.fetch = jest.fn(() => ({ json: () => Promise.resolve(json), text: () => Promise.resolve(rss) }));
    });

    afterEach(() => {
      global.fetch.mockClear();
    });

    it("fetches the resource from the right endpoint", async () => {
      const podcast = podcastGetterFactory({ cache });
      await podcast({ id: "123456789" });

      expect(global.fetch).toHaveBeenCalledWith(
        "https://cors-anywhere.herokuapp.com/https://itunes.apple.com/lookup?id=123456789",
      );
    });

    it("uses the feed URL to get the RSS document", async () => {
      const podcast = podcastGetterFactory({ cache });
      await podcast({ id: "123456789" });

      expect(global.fetch).toHaveBeenNthCalledWith(
        2,
        "https://cors-anywhere.herokuapp.com/http://feeds.soundcloud.com/users/soundcloud:users:187880289/sounds.rss",
      );
    });

    it("saves the feed JSON represenation into cache", async () => {
      const podcast = podcastGetterFactory({ cache });

      await podcast({ id: "123456789" });

      const feed = await new RssParser().parseString(rss);
      expect(cache.persist).toHaveBeenCalledWith({ id: "123456789", data: feed });
    });

    it("calls the Podcast factory to build the object", async () => {
      const spy = jest.spyOn(Podcast, "fromFeedJson");
      const podcast = podcastGetterFactory({ cache });

      await podcast({ id: "123456789" });

      const feed = await new RssParser().parseString(rss);

      expect(spy).toHaveBeenCalledWith("123456789", feed);

      spy.mockRestore();
    });

    describe("reading from cache", () => {
      beforeEach(async () => {
        const feed = await new RssParser().parseString(rss);

        cache = Object.assign({}, cache, { hasValidCachedData: () => true, getCachedData: () => feed });
      });

      it("does not perform HTTP requests if there are valid cached data", async () => {
        const podcast = podcastGetterFactory({ cache });
        await podcast({ id: "123456789" });

        expect(global.fetch).not.toHaveBeenCalled();
      });

      it("calls the Podcast factory to build the object", async () => {
        const spy = jest.spyOn(Podcast, "fromFeedJson");
        const podcast = podcastGetterFactory({ cache });

        await podcast({ id: "123456789" });

        const feed = await new RssParser().parseString(rss);

        expect(spy).toHaveBeenCalledWith("123456789", feed);

        spy.mockRestore();
      });
    });
  });
});
