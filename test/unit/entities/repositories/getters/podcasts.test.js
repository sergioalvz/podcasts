import { readFileSync } from "fs";
import { join } from "path";
import noop from "lodash/noop";

import { Podcast } from "../../../../../app/entities/Podcast";
import { podcastsGetterFactory } from "../../../../../app/entities/repositories/getters/podcasts";

describe("getter", () => {
  describe("podcasts", () => {
    const json = JSON.parse(readFileSync(join(__dirname, "../../../../fixtures/api/podcasts.json"), "utf-8"));

    beforeEach(() => {
      global.fetch = jest.fn(() => ({ json: () => Promise.resolve(json) }));
    });

    afterEach(() => {
      global.fetch.mockRestore();
    });

    it("fetches the collection of podcasts from the internet", async () => {
      const cache = { hasValidCachedData: () => false, persist: noop };

      const podcasts = podcastsGetterFactory({ cache });
      await podcasts();

      expect(global.fetch).toHaveBeenCalledWith(
        "https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json",
      );
    });

    it("saves the response into cache", async () => {
      const cache = { hasValidCachedData: () => false, persist: jest.fn() };

      const podcasts = podcastsGetterFactory({ cache });
      await podcasts();

      expect(cache.persist).toHaveBeenCalledWith({ data: json });
    });

    it("returns the right number of podcasts", async () => {
      const cache = { hasValidCachedData: () => false, persist: noop };

      const podcasts = podcastsGetterFactory({ cache });
      const collection = await podcasts();

      expect(collection).toHaveLength(100);
    });

    it("returns an array of Podcast objects", async () => {
      const cache = { hasValidCachedData: () => false, persist: noop };

      const podcasts = podcastsGetterFactory({ cache });
      const collection = await podcasts();

      expect(collection).toEqual(expect.arrayContaining([expect.any(Podcast)]));
    });

    describe("working with cached data", () => {
      it("does not perform HTTP requests if there are valid cached data", async () => {
        const cache = { hasValidCachedData: () => true, getCachedData: () => json, persist: noop };

        const podcasts = podcastsGetterFactory({ cache });
        await podcasts();

        expect(global.fetch).not.toHaveBeenCalled();
      });

      it("returns the right number of podcasts", async () => {
        const cache = { hasValidCachedData: () => true, getCachedData: () => json, persist: noop };

        const podcasts = podcastsGetterFactory({ cache });
        const collection = await podcasts();

        expect(collection).toHaveLength(100);
      });

      it("returns an array of Podcast objects", async () => {
        const cache = { hasValidCachedData: () => true, getCachedData: () => json, persist: noop };

        const podcasts = podcastsGetterFactory({ cache });
        const collection = await podcasts();

        expect(collection).toEqual(expect.arrayContaining([expect.any(Podcast)]));
      });
    });
  });
});
