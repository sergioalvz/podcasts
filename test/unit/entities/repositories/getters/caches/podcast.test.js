import noop from "lodash/noop";

import { podcast } from "../../../../../../app/entities/repositories/getters/caches/podcast";

describe("cache", () => {
  describe("podcast", () => {
    const id = "123456";

    describe("#hasValidCachedData", () => {
      it("uses the right storage path to get the data", () => {
        const storage = { getItem: jest.fn() };
        const podcastCache = podcast({ storage });

        podcastCache.hasValidCachedData({ id });

        expect(storage.getItem).toHaveBeenCalledWith("podcaster.podcast.123456");
      });

      it("returns false if the cache is empty", () => {
        const storage = { getItem: noop };
        const podcastCache = podcast({ storage });

        expect(podcastCache.hasValidCachedData({ id })).toBe(false);
      });

      it("returns true if the cache has valid data", () => {
        const cachedData = { lastUpdateAt: new Date("2018-11-03"), data: {} };
        const storage = { getItem: () => JSON.stringify(cachedData) };

        const podcastCache = podcast({ storage, today: new Date("2018-11-03") });

        expect(podcastCache.hasValidCachedData({ id })).toBe(true);
      });

      it("returns false if the cache has expired data", () => {
        const cachedData = { lastUpdateAt: new Date("2018-11-03"), data: {} };
        const storage = { getItem: () => JSON.stringify(cachedData) };

        const podcastCache = podcast({ storage, today: new Date("2018-11-04") });

        expect(podcastCache.hasValidCachedData({ id })).toBe(false);
      });
    });

    describe("#getCachedData", () => {
      it("uses the right storage path to get the data", () => {
        const storage = { getItem: jest.fn() };
        const podcastCache = podcast({ storage });

        podcastCache.getCachedData({ id });

        expect(storage.getItem).toHaveBeenCalledWith("podcaster.podcast.123456");
      });

      it("returns undefined if the cache is empty", () => {
        const storage = { getItem: noop };
        const podcastCache = podcast({ storage });

        const cachedData = podcastCache.getCachedData({ id });

        expect(cachedData).not.toBeDefined();
      });

      it("returns JSON object if there is data", () => {
        const storage = { getItem: () => JSON.stringify({ lastUpdateAt: new Date(), data: { foo: "bar" } }) };
        const podcastCache = podcast({ storage });

        const cachedData = podcastCache.getCachedData({ id });

        expect(cachedData).toEqual({ foo: "bar" });
      });
    });

    describe("#persist", () => {
      it("saves the data with the expected scheme", () => {
        const today = new Date();
        const storage = { setItem: jest.fn() };
        const podcastCache = podcast({ storage, today });

        podcastCache.persist({ id, data: { foo: "bar" } });

        expect(storage.setItem).toHaveBeenCalledWith(
          "podcaster.podcast.123456",
          JSON.stringify({
            lastUpdateAt: today,
            data: { foo: "bar" },
          }),
        );
      });

      it("throws an exception if data is undefiend", () => {
        const today = new Date();
        const storage = { setItem: jest.fn() };
        const podcastCache = podcast({ storage, today });

        expect(() => podcastCache.persist({ id })).toThrowError();
      });

      it("throws an exception if data is not an object", () => {
        const today = new Date();
        const storage = { setItem: jest.fn() };
        const podcastCache = podcast({ storage, today });

        expect(() => podcastCache.persist({ id, data: 3 })).toThrowError();
      });

      it("throws an exception if there is no id", () => {
        const today = new Date();
        const storage = { setItem: jest.fn() };
        const podcastCache = podcast({ storage, today });

        expect(() => podcastCache.persist({ data: { foo: "bar" } })).toThrowError();
      });

      it("does not throw anything if there is an exception while saving data", () => {
        const storage = {
          setItem: () => {
            throw new Error("BOOM!");
          },
        };

        const podcastCache = podcast({ storage, today: new Date() });

        expect(() => podcastCache.persist({ id, data: { foo: "bar" } })).not.toThrowError();
      });
    });
  });
});
