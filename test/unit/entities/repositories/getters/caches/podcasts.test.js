import noop from "lodash/noop";

import { podcasts } from "../../../../../../app/entities/repositories/getters/caches/podcasts";

describe("cache", () => {
  describe("podcasts", () => {
    describe("#hasValidCachedData", () => {
      it("uses the right storage path to get the data", () => {
        const storage = { getItem: jest.fn() };
        const podcastsCache = podcasts({ storage });

        podcastsCache.hasValidCachedData();

        expect(storage.getItem).toHaveBeenCalledWith("podcaster.podcasts");
      });

      it("returns false if the cache is empty", () => {
        const storage = { getItem: noop };
        const podcastsCache = podcasts({ storage });

        expect(podcastsCache.hasValidCachedData()).toBe(false);
      });

      it("returns true if the cache has valid data", () => {
        const cachedData = { lastUpdateAt: new Date("2018-11-03"), data: {} };
        const storage = { getItem: () => JSON.stringify(cachedData) };

        const podcastsCache = podcasts({ storage, today: new Date("2018-11-03") });

        expect(podcastsCache.hasValidCachedData()).toBe(true);
      });

      it("returns false if the cache has expired data", () => {
        const cachedData = { lastUpdateAt: new Date("2018-11-03"), data: {} };
        const storage = { getItem: () => JSON.stringify(cachedData) };

        const podcastsCache = podcasts({ storage, today: new Date("2018-11-04") });

        expect(podcastsCache.hasValidCachedData()).toBe(false);
      });
    });

    describe("#getCachedData", () => {
      it("uses the right storage path to get the data", () => {
        const storage = { getItem: jest.fn() };
        const podcastsCache = podcasts({ storage });

        podcastsCache.getCachedData();

        expect(storage.getItem).toHaveBeenCalledWith("podcaster.podcasts");
      });

      it("returns undefined if the cache is empty", () => {
        const storage = { getItem: noop };
        const podcastsCache = podcasts({ storage });

        const cachedData = podcastsCache.getCachedData();

        expect(cachedData).not.toBeDefined();
      });

      it("returns JSON object if there is data", () => {
        const storage = { getItem: () => JSON.stringify({ lastUpdateAt: new Date(), data: { foo: "bar" } }) };
        const podcastsCache = podcasts({ storage });

        const cachedData = podcastsCache.getCachedData();

        expect(cachedData).toEqual({ foo: "bar" });
      });
    });

    describe("#persist", () => {
      it("saves the data with the expected scheme", () => {
        const today = new Date();
        const storage = { setItem: jest.fn() };
        const podcastsCache = podcasts({ storage, today });

        podcastsCache.persist({ data: { foo: "bar" } });

        expect(storage.setItem).toHaveBeenCalledWith(
          "podcaster.podcasts",
          JSON.stringify({
            lastUpdateAt: today,
            data: { foo: "bar" },
          }),
        );
      });

      it("throws an exception if data is undefiend", () => {
        const today = new Date();
        const storage = { setItem: jest.fn() };
        const podcastsCache = podcasts({ storage, today });

        expect(() => podcastsCache.persist({})).toThrowError("Invalid data to persist");
      });

      it("throws an exception if data is not an object", () => {
        const today = new Date();
        const storage = { setItem: jest.fn() };
        const podcastsCache = podcasts({ storage, today });

        expect(() => podcastsCache.persist({ data: 3 })).toThrowError("Invalid data to persist");
      });

      it("does not throw anything if there is an exception while saving data", () => {
        const storage = {
          setItem: () => {
            throw new Error("BOOM!");
          },
        };

        const podcastCache = podcasts({ storage, today: new Date() });

        expect(() => podcastCache.persist({ data: { foo: "bar" } })).not.toThrowError();
      });
    });
  });
});
