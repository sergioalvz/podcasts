import React from "react";
import { shallow } from "enzyme";
import noop from "lodash/noop";

import { SearchBox } from "../../../../app/components/SearchBox";

describe("SearchBox", () => {
  it("renders the number of results", () => {
    const subject = shallow(<SearchBox numberOfResults={3} onChange={noop} placeholder="whatever" />);

    expect(subject.find('[data-test-id="results"]').text()).toEqual("3");
  });

  it("calls the onChange prop on every input change", () => {
    const spy = jest.fn();

    const subject = shallow(<SearchBox numberOfResults={3} onChange={spy} placeholder="whatever" />);
    subject.find('[data-test-id="input"]').simulate("change");

    expect(spy).toHaveBeenCalled();
  });
});
