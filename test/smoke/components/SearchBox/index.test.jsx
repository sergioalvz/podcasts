import React from "react";
import { shallow } from "enzyme";
import noop from "lodash/noop";

import { SearchBox } from "../../../../app/components/SearchBox";

describe("SearchBox", () => {
  it("renders without errors", () => {
    expect(() => shallow(<SearchBox numberOfResults={4} onChange={noop} placeholder="Foo" />)).not.toThrowError();
  });
});
