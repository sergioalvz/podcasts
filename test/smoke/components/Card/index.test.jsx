import React from "react";
import { shallow } from "enzyme";

import { Card } from "../../../../app/components/Card";

describe("Card", () => {
  it("renders without errors", () => {
    expect(() => shallow(<Card>Foo</Card>)).not.toThrowError();
  });
});
