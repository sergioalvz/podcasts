import React from "react";
import { shallow } from "enzyme";

import { Loader } from "../../../../app/components/Loader";

describe("Loader", () => {
  it("renders without errors", () => {
    expect(() => shallow(<Loader label="Loading..." />)).not.toThrowError();
  });
});
