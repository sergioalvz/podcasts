import React from "react";
import { shallow } from "enzyme";

import { Episode } from "../../../../app/screens/Episode";
import { MockPodcastRepository } from "../../../mocks/MockPodcastRepository";

describe("Episode", () => {
  it("renders without errors", (done) => {
    const props = {
      match: { params: { podcastId: "1234", episodeId: "1234" } },
      podcastRepository: new MockPodcastRepository(),
    };

    // HACK: since Episode#componentDidMount is an async method, we need to wait
    // until all the stacked operations are done to ensure a proper render validation
    const subject = shallow(<Episode {...props} />);
    setImmediate(() => {
      expect(() => subject.update()).not.toThrowError();
      done();
    });
  });
});
