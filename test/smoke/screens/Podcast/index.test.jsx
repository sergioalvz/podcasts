import React from "react";
import { shallow } from "enzyme";

import { Podcast } from "../../../../app/screens/Podcast";
import { MockPodcastRepository } from "../../../mocks/MockPodcastRepository";

describe("Podcast", () => {
  it("renders without errors", (done) => {
    const props = {
      match: { params: { podcastId: "1234" } },
      podcastRepository: new MockPodcastRepository(),
    };

    // HACK: since Podcast#componentDidMount is an async method, we need to wait
    // until all the stacked operations are done to ensure a proper render validation
    const subject = shallow(<Podcast {...props} />);
    setImmediate(() => {
      expect(() => subject.update()).not.toThrowError();
      done();
    });
  });
});
