import React from "react";
import { shallow } from "enzyme";

import { Podcasts } from "../../../../app/screens/Podcasts";
import { MockPodcastRepository } from "../../../mocks/MockPodcastRepository";

describe("Podcasts", () => {
  it("renders without errors", (done) => {
    const props = { podcastRepository: new MockPodcastRepository() };

    // HACK: since Podcasts#componentDidMount is an async method, we need to wait
    // until all the stacked operations are done to ensure a proper render validation
    const subject = shallow(<Podcasts {...props} />);
    setImmediate(() => {
      expect(() => subject.update()).not.toThrowError();
      done();
    });
  });
});
