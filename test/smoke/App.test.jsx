import React from "react";
import { shallow } from "enzyme";

import { App } from "../../app/App";
import { MockPodcastRepository } from "../mocks/MockPodcastRepository";

describe("App", () => {
  it("renders without errors", () => {
    expect(() => shallow(<App podcastRepository={new MockPodcastRepository()} />)).not.toThrowError();
  });
});
