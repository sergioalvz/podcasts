import { Podcast } from "../../app/entities/Podcast";
import { EpisodeFactory } from "./Episode";

export function PodcastFactory(overrides = {}) {
  const defaults = {
    name: "My awesome podcast",
    artist: "Awesome Artist",
    image: "https://via.placeholder.com/170x170",
    id: "12397134",
    episodes: [EpisodeFactory()],
    description: "This is an awesome podast that is delivered weekly",
  };

  return new Podcast({ ...defaults, ...overrides });
}
