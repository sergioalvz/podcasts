import { Episode } from "../../app/entities/Episode";

export function EpisodeFactory(overrides = {}) {
  const defaults = {
    description: "The best episode of the whole collection",
    duration: "23:43",
    id: "episode-number-2",
    podcastId: "123",
    releaseDate: new Date(2018, 11, 31),
    title: "Episode number 2",
    url: "http://example.com",
  };

  return new Episode({ ...defaults, ...overrides });
}
