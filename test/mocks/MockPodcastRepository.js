import { PodcastRepository } from "../../app/entities/repositories/PodcastRepository";
import { PodcastFactory } from "../factories/Podcast";

export class MockPodcastRepository extends PodcastRepository {
  constructor() {
    super({});
  }

  all() {
    return Promise.resolve([PodcastFactory()]);
  }

  findById() {
    return Promise.resolve(PodcastFactory());
  }

  search() {
    return Promise.resolve([PodcastFactory()]);
  }
}
